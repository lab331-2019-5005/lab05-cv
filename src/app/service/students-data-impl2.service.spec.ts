import { TestBed, inject } from '@angular/core/testing';

import { StudentsDataImpl2Service } from './students-data-impl2.service';

describe('StudentsDataImpl2Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentsDataImpl2Service]
    });
  });

  it('should be created', inject([StudentsDataImpl2Service], (service: StudentsDataImpl2Service) => {
    expect(service).toBeTruthy();
  }));
});
